import os
import sys
import examples
from game_setup import *
import glob
import random
import tensorflow as tf
import numpy as np
#tf.compat.v1.disable_eager_execution()


NUM_FEATURES = 40
NUM_ARRAY_FEATURES = 5


def epoch_from_checkpoint_path(checkpoint_path):
  # Checkpoint path has form: dir/weights.{epoch:02d}-{loss:.2f}.hdf5
  # For e.g.: /checkpoint/weights.06-4.69.hdf5
  filename = os.path.basename(checkpoint_path)
  epoch_num_start = filename.find('.') + 1
  epoch_num_end = filename.rfind('-')
  return int(filename[epoch_num_start:epoch_num_end])

def batch_matmul(E, F):
  """Matmul for each item in the batch.

  Args:
    E: edge adjacency, shape: num edges x num edges
    F: features: batch size x num edges x num features

  Returns:
    EF of size batch size x num edges x num features
  """
  # [e, batch_size, f]
  #EF = tf.tensordot(E, F, axes=[[1], [1]])
  # [batch_size, e, f]
  #return tf.transpose(EF, [1, 0, 2])
  #return EF
  return tf.einsum('nm,bmk->bnk', E, F)

class GraphLayer(tf.keras.layers.Layer):
  def __init__(
      self, E, num_edges, num_outputs, multiply_by_E=True,
      kernel_regularizer=None,
      bias_regularizer=None,
      activation=None,
      **kwargs):
    super(GraphLayer, self).__init__(**kwargs)
    self.E = tf.convert_to_tensor(E)
    self.num_edges = num_edges
    self.num_outputs = num_outputs
    self.multiply_by_E = multiply_by_E
    self.kernel_regularizer = kernel_regularizer
    self.bias_regularizer = bias_regularizer
    self.activation = activation

  def build(self, input_shape):
    self.kernel = self.add_weight(name='graph_kernel',
                                  shape=(int(input_shape[-1]), self.num_outputs),
                                  initializer='uniform',
                                  regularizer=self.kernel_regularizer,
                                  trainable=True)
    super(GraphLayer, self).build(input_shape)

  def call(self, inputs):
    # input shape: batch, number of edges, number of features
    output = tf.convert_to_tensor(inputs)
    if self.multiply_by_E:
      output = batch_matmul(self.E, output)
    output = tf.einsum('bnm,mw->bnw', output, self.kernel)
    if self.activation:
      output = self.activation(output)
    return output

  def compute_output_shape(self, input_shape):
    shape = tf.TensorShape(input_shape).as_list()
    shape[-1] = self.num_outputs
    return tf.TensorShape(shape)


class TicketToRideModel(tf.keras.Model):

  def __init__(self, E, num_actions, num_edges):
    super(TicketToRideModel, self).__init__(name='TicketToRide')
    self.num_actions = num_actions
    self.num_edges = num_edges
    self.reg = 0 #0.001
    num_graph_outputs = NUM_FEATURES
    self.graph1 = GraphLayer(
        E, num_edges, num_graph_outputs, multiply_by_E=False,
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        activation=tf.nn.leaky_relu,
        input_shape=(num_edges, NUM_FEATURES))

    self.batch_norm1 = tf.keras.layers.BatchNormalization(-1)
    self.graph2 = GraphLayer(
        E, num_edges, num_graph_outputs,
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        activation=tf.nn.leaky_relu)
    self.batch_norm2 = tf.keras.layers.BatchNormalization(-1)
    self.graph3 = GraphLayer(
        E, num_edges, num_graph_outputs,
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        activation=tf.nn.leaky_relu)

    self.batch_norm3 = tf.keras.layers.BatchNormalization(-1)
    self.graph4 = GraphLayer(
        E, num_edges, 6, #16
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        activation=tf.nn.leaky_relu)
    self.graph5 = GraphLayer(
        E, num_edges, 1,
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        activation=tf.nn.leaky_relu)

    self.batch_norm4 = tf.keras.layers.BatchNormalization(-1)
    self.flatten1 = tf.keras.layers.Flatten()
    self.dense1 = tf.keras.layers.Dense(
        512,
        activation=tf.nn.leaky_relu,
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        kernel_initializer=tf.keras.initializers.RandomUniform())
    self.batch_norm5 = tf.keras.layers.BatchNormalization(-1)
    self.dense3 = tf.keras.layers.Dense(
        num_actions,
        activation=tf.nn.softmax,
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        kernel_initializer=tf.keras.initializers.RandomUniform())
    self.dense4 = tf.keras.layers.Dense(
        16,
        activation=tf.nn.leaky_relu, # map value into (-1, 1) region
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        kernel_initializer=tf.keras.initializers.RandomUniform())
    self.dense5 = tf.keras.layers.Dense(
        1,
        activation=tf.nn.tanh, # map value into (-1, 1) region
        kernel_regularizer=tf.keras.regularizers.l2(self.reg),
        kernel_initializer=tf.keras.initializers.RandomUniform())
    self.concat = tf.keras.layers.Concatenate(axis=1)

  def call(self, inputs, training=False):
    # Inputs should be a tuple of:
    # graph inputs, array inputs
    graph_inputs, array_inputs = inputs
    out = self.graph1(graph_inputs)
    #out = self.batch_norm1(out, training=training)
    out = self.graph2(out)
    #out = self.batch_norm2(out, training=training)
    out = self.graph3(out)
    #out = self.batch_norm3(out, training=training)
    #out = self.graph4(out)

    out = self.flatten1(out)

    #out = self.batch_norm4(out, training=training)
    #out = self.dense1(out)
    #out = self.batch_norm5(out, training=training)
    policy = self.dense3(out)
    value = self.dense4(out)
    value = self.dense5(value)
    return [policy, value]

  def compute_output_shape(self, input_shape):
    return [tf.TensorShape([None, self.num_actions]), tf.TensorShape([None, 1])]


def tf_model(board):
  E = edge_adjacency_matrix(board)

  model = TicketToRideModel(E, board.numActions(), len(board.roads))
  #sgd = tf.keras.optimizers.SGD(lr=0.01, decay=1e-3, momentum=0.5, nesterov=True)
  adam = tf.keras.optimizers.Adam(learning_rate=0.001)
  model.compile(optimizer=adam,
                #loss=['categorical_crossentropy', 'MSE'],
                loss=['KLD', 'MSE'],
                loss_weights=[0.7, 1],
                run_eagerly=True)
  return model


class TicketToRideNN(object):
  def __init__(self, board, checkpoint_directory, tensorboard_directory):
    self.checkpoint_directory = checkpoint_directory
    self.checkpoint_filepath = None
    if checkpoint_directory:
      self.checkpoint_filepath = os.path.join(
          os.path.expanduser(checkpoint_directory),
          'weights.{epoch:02d}-{loss:.2f}')
    self.tensorboard_directory = None
    if tensorboard_directory:
      self.tensorboard_directory = os.path.expanduser(tensorboard_directory)
    self.board = board
    self.trained = False
    self.num_edges = len(board.roads)
    self.epoch = 0
    self.model = tf_model(board)
    self.restore_checkpoint()
    adam = tf.keras.optimizers.Adam(learning_rate=0.001)
    self.model.compile(optimizer=adam,
                loss=['KLD', 'MSE'],
                loss_weights=[0.7, 1])

    self.tensorboard_callback = None
    if self.tensorboard_directory:
      self.tensorboard_callback = tf.keras.callbacks.TensorBoard(
          log_dir=self.tensorboard_directory,
          profile_batch=0,
          write_graph=True,
          histogram_freq=5,
          update_freq=5)
    self.checkpoint_callback = None
    if self.checkpoint_directory:
      self.checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
          filepath=self.checkpoint_filepath,
          save_weights_only=True,
          save_freq='epoch')

  def predict(self, state):
    import time
    t1 = time.time()
    graph_features, array_features = examples.encode_game_state(state)
    graph_features = np.reshape(graph_features, (1, graph_features.shape[0], graph_features.shape[1]))
    array_features = np.reshape(array_features, (1, array_features.shape[0]))
    t2 = time.time()

    outputs = self.model.predict([graph_features, array_features])
    t3 = time.time()
    #print('encode: %s, predict: %s' % (str(t2 - t1), str(t3 - t2)))
    assert len(outputs) == 2
    assert outputs[0].shape == (1, self.board.numActions())
    assert outputs[1].shape == (1, 1), 'actual shape: %s' % str(outputs[1].shape)
    value = outputs[1][0][0]
    return outputs[0].flatten(), value

  def callbacks(self):
    callbacks = []
    if self.tensorboard_callback:
      callbacks.append(self.tensorboard_callback)
    if self.checkpoint_callback:
      callbacks.append(self.checkpoint_callback)
    return callbacks

  def train(self, examples):
    """Each example is a list of states from one game."""
    single_examples = []
    for example_list in examples:
      single_examples.extend(example_list)
    examples = single_examples
    
    # Sample number of examples divisible by batch_size
    batch_size = 128
    num_examples = batch_size * int(len(examples) / batch_size)
    num_examples = min(num_examples, 5120)
    examples = random.sample(examples, num_examples)
    # Each example should be a tuple:
    # (state, action, average reward)
    graph_inputs = np.zeros((len(examples), self.num_edges, NUM_FEATURES), dtype=np.float32)
    array_inputs = np.zeros((len(examples), NUM_ARRAY_FEATURES), dtype=np.float32)
    expected_policy = np.zeros((len(examples), self.board.numActions()), dtype=np.float32)
    expected_value = np.zeros((len(examples), 1), dtype=np.float32)

    index = 0
    for example in examples:
      state, policy, value = example
      graph_inputs[index], array_inputs[index] = examples.encode_game_state(state)
      expected_policy[index] = np.array(policy)
      expected_value[index][0] = value
      index += 1

    epochs_per_train_call = 1
    history = self.model.fit([graph_inputs, array_inputs],
                             [expected_policy, expected_value],
                             batch_size=batch_size,
                             validation_split=0.1,
                             epochs=self.epoch + epochs_per_train_call,
                             initial_epoch=self.epoch,
                             callbacks=self.callbacks())
    self.epoch += epochs_per_train_call
    self.trained = True
    self.cleanup_old_checkpoints()
    print('History: %s' % str(history.history))

  def pretrain(self, dataset):
    """Each example is a list of states from one game."""
    print('Pretraining')
    # Sample number of examples divisible by batch_size
    batch_size = 512
    dataset = dataset.batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    history = self.model.fit(dataset,
                             epochs=self.epoch + 200,
                             initial_epoch=self.epoch,
                             verbose=0,
                             callbacks=self.callbacks())
    self.trained = True

  def eval(self, dataset):
    print('Eval')
    # Sample number of examples divisible by batch_size
    batch_size = 256
    dataset = dataset.batch(batch_size)
    history = self.model.evaluate(dataset)

  def cleanup_old_checkpoints(self):
    checkpoint_pattern1 = os.path.join(
        os.path.expanduser(self.checkpoint_directory), 'weights.*')

    keep = 20  # how many checkpoints to keep
    list_of_files = glob.glob(checkpoint_pattern1)
    if len(list_of_files) <= keep:
      return
    remove_count = len(list_of_files) - keep
    oldest = sorted(list_of_files, key=os.path.getctime)[:remove_count]
    for f in oldest:
      #index_f =  f[:f.rfind('.')] + '.index'
      print('removing %s' % f)
      os.remove(f)

  def restore_checkpoint(self):
    if not self.checkpoint_filepath:
      print('No checkpoint directory specified.')
      return
    checkpoint_dir = os.path.normpath(
            os.path.dirname(self.checkpoint_filepath)) + '/'
    latest_checkpoint = tf.train.latest_checkpoint(checkpoint_dir)
    if not latest_checkpoint:
      print('No checkpoint found at %s' % checkpoint_dir)
      return
    self.model.load_weights(latest_checkpoint)
    print('Checkpoint filepath: %s' % self.checkpoint_filepath)
    print('Restoring checkpoint: %s' % latest_checkpoint)
    self.epoch = epoch_from_checkpoint_path(latest_checkpoint)
    print('Current epoch: %s' % self.epoch)
    self.trained = True
