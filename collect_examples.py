import argparse
import data
import examples
from game_setup import *
from model import *
import mcts_nn
import os

class Game(object):
  def __init__( self, agents):
    self.agents = agents

  def run(self):
    board = getDefaultBoard()
    current_player = 0
    player_count = 2

    state = initialState(board, player_count, start_player=current_player)
    action = None
    current_agent = None
    agent_times = [0, 0]
    agent_actions = [0, 0]
    ex = examples.Examples()

    i = 0
    while True:
      i += 1
      if i > 100:
        # Something must be wrong if the game takes so long
        assert False

      legal_actions = state.getLegalActions()
      if not legal_actions:
        break

      current_agent = self.agents[state.current_player]

      start_time = time.time()
      action, policy, Q, N = current_agent.pickAction(state, legal_actions, return_policy=True)
      agent_times[state.current_player] += time.time() - start_time
      agent_actions[state.current_player] += 1
      ex.add(state.getVisibleState(), policy)
      state = state.generateSuccessor(action)

      if state.isEnd():
        break
    winner, tie = getWinner(state)
    ex.update_outcomes(winner, tie, Q, N, board.numActions())
    return ex.examples


def collect(num_games, filename):
  agent1 = mcts_nn.MctsNN()
  agent2 = mcts_nn.MctsNN()

  games_per_file = 100
  print('Games: %d' % num_games)
  file_count = num_games // games_per_file
  print('File count: %d' % file_count)

  for file_index in range(file_count):
    example_list = []
    for i in range(games_per_file):
      game = Game([agent1, agent2])
      example_list.extend(game.run())

    # Convert examples to NN inputs
    graph_features_list = []
    array_features_list = []
    policy_list = []
    value_list = []

    for example in example_list:
      state = example[0]
      policy = example[1]
      value = example[2]
      graph_features, array_features = examples.encode_game_state(state)
      graph_features_list.append(graph_features)
      array_features_list.append(array_features)
      policy_list.append(policy)
      value_list.append(value)

    data.save_examples(
        graph_features_list, array_features_list, policy_list, value_list,
        os.path.join(filename, 'data%d' % file_index))


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Collect by running games between MCTS agents.')
  parser.add_argument('-g', '--games', default=1000, type=int)
  parser.add_argument('-f', '--filepath', default='~/dataset/', type=str)
  args = parser.parse_args()

  collect(args.games, os.path.expanduser(args.filepath))
 
