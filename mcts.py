from game_setup import *
from model import *
import agent
import collections
import math
import random


UCB_CONSTANT = math.sqrt(2)

def getWinner(state):
  """Returns (winner, tie) tuple for current state of the game."""
  scores = state.calculateScores()
  if scores.count(scores[0]) == len(scores):
    return -1, True
  winner = None
  winning_score = -1
  for player in range(len(scores)):
    score = scores[player]
    if winning_score == -1 or score > winning_score:
      winner = player
      winning_score = score
  return winner, False


class Mcts(agent.Agent):
  def __init__(self, reset_after_actions=False):
    self.game_count = collections.defaultdict(int)
    # Setting number of wins to be a float so that we can
    # set it to 0.5 in case of a tie.
    self.win_count = collections.defaultdict(float)
    # How many move to expand
    # play each game to the end in simulation
    self.max_moves = 100

    self.player_count = 2
    self.board = getDefaultBoard()
    self.first_action = None
    self.reset_after_actions = reset_after_actions

  def reset(self):
    self.game_count = collections.defaultdict(int)
    self.win_count = collections.defaultdict(float)

  def pickAction(self, state, legal_actions):
    player = state.current_player

    if not legal_actions:
      return None
    if len(legal_actions) == 1:
      return legal_actions[0]

    games = 0
    while games < 100:
      self.run_simulation(state)
      games += 1

    next_states = [state.generateSuccessor(a) for a in legal_actions]

    def percent_wins(state):
      state_key = (player, str(state.getVisibleState()))
      wins = self.win_count.get(state_key, 0)
      games = float(self.game_count.get(state_key, 1))
      # Laplace smoothing
      if wins > 0:
        wins += 1.0
        games += 2.0
      return wins / games


    all_percent_wins = []
    if len(next_states) < 6:
      all_percent_wins = [percent_wins(s) for s in next_states]
    percent_wins, action_index = max(
        (percent_wins(s), i) for i, s in enumerate(next_states))
    state_key = (player, str(next_states[action_index]))

    if self.reset_after_actions:
      self.reset()

    action = legal_actions[action_index]
    return action

  def pick_action_with_ucb1(self, player, state_keys):
    """Calculate ucb1.

    Args:
      player: player
      state_keys: visible state ids.
    """
    wins = [self.win_count.get(s, 0) for s in state_keys]
    plays = [float(self.game_count.get(s, 0)) for s in state_keys]
    if min(plays) == 0:
      return -1

    def ucb1(w, n, logN):
      return w/n + UCB_CONSTANT*math.sqrt(logN/n)

    log_total_plays = math.log(sum(plays))
    value, index = max((ucb1(wins[i], plays[i], log_total_plays), i)
                       for i in range(len(plays)))
    assert len(state_keys[index]) == 2
    return index


  def run_simulation(self, state):
    # play out a random game and update statistics
    state = state.deepCopy()
    visited_states = set()
    player = state.current_player
    winner = -1
    tie = False

    visible_state = state.getVisibleState()
    assert state.getVisibleState() == visible_state
    expand = True
    for t in range(self.max_moves):
      legal_actions = state.getLegalActions()
      # if no legal actions left, consider this the end of the game
      if not legal_actions:
        # print('No legal actions')
        winner, tie = getWinner(state)
        break

      next_states = [state.generateSuccessor(a) for a in legal_actions]
      #next_state_keys = [(player, str(s)) for s in next_states]
      next_state_keys = [(player, str(s.getVisibleState())) for s in next_states]

      action_index = self.pick_action_with_ucb1(player, next_state_keys)
      if action_index < 0:
        action_index = random.choice(range(len(next_states)))
      state = next_states[action_index]
      state_key = next_state_keys[action_index]
      assert len(state_key) == 2

      # `player` here and below refers to the player
      # who moved into that particular state.
      if expand and state_key not in self.game_count:
          expand = False
          self.game_count[state_key] = 0
          self.win_count[state_key] = 0

      visited_states.add(state_key)

      player = state.current_player
      if state.isEnd():
        # print('End')
        winner, tie = getWinner(state)
        break

    incremented = False
    for state_key in visited_states:
      assert len(state_key) == 2
      if state_key not in self.game_count:
        continue
      self.game_count[state_key] += 1
      player = state_key[0]
      incremented = True
      if tie:
        # print('Tie!')
        self.win_count[state_key] += 0.5
      elif player == winner:
        self.win_count[state_key] += 1
    #assert max(self.win_count.values()) > 0, 'winner=%d, tie=%s' % (winner, str(tie))


