
class Agent(object):
  """Interface for agent implementations."""
  def pickAction(self, state, legal_actions):
    raise NotImplementedError(
        "pickAction should be implemented by subclasses of Agent.")

