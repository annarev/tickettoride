# Utilities for saving data
import glob
import numpy as np
import tensorflow as tf

SHUFFLE_BUFFER_SIZE = 410421

def save_examples(
    graph_feature_list, array_feature_list, policy_list, value_list, filename):
  np.save('%s_graph.npy' % filename, graph_feature_list)
  np.save('%s_array.npy' % filename, array_feature_list)
  np.save('%s_policy.npy' % filename, policy_list)
  np.save('%s_value.npy' % filename, value_list)

def load_examples(filename):
  graph_feature_list = np.load('%s_graph.npy' % filename)
  array_feature_list = np.load('%s_array.npy' % filename)
  policy_list = np.load('%s_policy.npy' % filename)
  value_list = np.load('%s_value.npy' % filename)
  print('Loading %s with %d examples ...' % (filename, graph_feature_list.shape[0]))
  dataset = tf.data.Dataset.from_tensor_slices(
      (graph_feature_list, array_feature_list, policy_list, value_list))
  dataset = dataset.map(lambda g,a,p,v: ((g, a), (p, v)))
  return dataset

def load_multiple_examples(filename):
  datasets = []
  for i in range(50):
    path = '%s%d' % (filename, i)
    d = load_examples(path)
    datasets.append(d)

  choice_dataset = tf.data.Dataset.range(len(datasets)).repeat(len(datasets))
  dataset = datasets[0]
  for i in range(1, len(datasets)):
    assert i != 0
    dataset = dataset.concatenate(datasets[i])

  dataset = dataset.shuffle(SHUFFLE_BUFFER_SIZE)
    
  print('Dataset size: %d' % tf.data.experimental.cardinality(dataset))
  return dataset


