import os
from game_setup import *
from model import *
import agent
import argparse
import mcts
import mcts_nn
import random
import time
import visual

class FirstActionAgent(agent.Agent):
  def pickAction(self, state, legal_actions):
    if not legal_actions:
      return None
    return legal_actions[0]

class RandomAgent(agent.Agent):
  def pickAction(self, state, legal_actions):
    if not legal_actions:
      return None
    return random.choice(legal_actions)

class BuildLongestRoadAgent(agent.Agent):
  def pickAction(self, state, legal_actions):
    if not legal_actions:
      return None
    longest_route_action = None
    longest_route_length = 0

    for action in legal_actions:
      if (action.action_type == ActionType.CLAIM_ROUTE and
          action.road.distance > longest_route_length):
        longest_route_action = action
        longest_route_length = action.road.distance

    if longest_route_action:
      return longest_route_action
    return legal_actions[0]

class OracleAgent(agent.Agent):
  def __init__(self):
    # DestinationCard(CITY_DENVER, CITY_PITTSBURGH, 11),
    # DestinationCard(CITY_LOS_ANGELES, CITY_MIAMI, 20),
    # DestinationCard(CITY_BOSTON, CITY_MIAMI, 12),
    self.preferred_cities = {
      CITY_LOS_ANGELES, CITY_PHOENIX,
      CITY_SANTA_FE, CITY_DENVER, CITY_KANSAS_CITY, CITY_SAINT_LOUIS, CITY_NASHVILLE,
      CITY_ATLANTA, CITY_MIAMI, CITY_CHICAGO, CITY_PITTSBURGH, CITY_TORONTO,
      CITY_NEW_YORK, CITY_BOSTON, CITY_MONTREAL
    }


  def pickAction(self, state, legal_actions):
    selected_action = None
    longest_route = 0
    for action in legal_actions:
       if action.action_type == ActionType.CLAIM_ROUTE:
         if (action.road.city1 in self.preferred_cities and
             action.road.city2 in self.preferred_cities and
             action.road.color != Color.GRAY and
             action.road.distance > longest_route):
            longest_route = action.road.distance
            selected_action = action
    if selected_action:
      return selected_action

    if legal_actions[0].action_type != ActionType.CLAIM_ROUTE:
      return legal_actions[0]

    #for action in legal_actions:
    #   if action.action_type == ActionType.CLAIM_ROUTE:
    #     if (action.road.city1 in self.preferred_cities and
    #         action.road.city2 in self.preferred_cities):
    #       return action
    return legal_actions[0]


class ConnectedCityAgent(agent.Agent):
  def __init__(self):
    self.preferred_cities = set()

  def pickAction(self, state, legal_actions):
    if not legal_actions:
      return None

    if not self.preferred_cities:
      for destination in state.player_to_destination_cards[state.current_player]: 
        self.preferred_cities.add(destination.city1)
        self.preferred_cities.add(destination.city2)
        break

    for action in legal_actions:
      if (action.action_type == ActionType.CLAIM_ROUTE and
          (action.road.city1 in self.preferred_cities or
           action.road.city2 in self.preferred_cities)):
        self.preferred_cities.add(action.road.city1)
        self.preferred_cities.add(action.road.city2)
        return action
    return legal_actions[0]


class Game(object):
  def __init__(self, agents, agent1label, agent2label, reportdir=None):
    self.agents = agents
    self.report = None
    if reportdir is not None:
      self.report = visual.GameReport(
          reportdir, agent1label, agent2label)

  def run(self, verbose=False):
    board = getDefaultBoard()
    current_player = 0
    player_count = 2
    player_to_destination_cards = {}

    state = initialState(board, player_count, start_player=current_player)
    action = None
    current_agent = None
    #print(str(self.agents))
    #print(state)
    agent_times = [0, 0]
    agent_actions = [0, 0]

    i = 0
    while True:
      if verbose:
        print('Iteration %d' % i)
      i += 1
      if i > 100:
        # Something must be wrong if the game takes so long
        assert False
      if verbose:
        print(str(state))
      if i % 2 == 1:
        scores = state.calculateScores()
        #print(scores)


      legal_actions = state.getLegalActions()
      if not legal_actions:
        break

      current_agent = self.agents[state.current_player]

      start_time = time.time()
      action = current_agent.pickAction(state, legal_actions)
      agent_times[state.current_player] += time.time() - start_time
      agent_actions[state.current_player] += 1
      
      if self.report:
        self.report.add(state.getVisibleState(), action)

      if action and verbose:
        print(str(action.action_type))
      state = state.generateSuccessor(action)

      if state.isEnd():
        break
    scores = state.calculateScores()
    print('GAME ENDED! %s' % str(scores))
    print('Times %s' % str([agent_times[i]/agent_actions[i] for i in range(2)]))
    if self.report:
      print('Report: %s ' % self.report.build(scores))
    return scores

def get_agent_for_name(name, other_agent_name, checkpoint_dir):
  if name == 'MCTS':
    # MCTSNN resets its state after each move. So, it is fair
    # if MCTS agent does the same for comparison.
    agent = mcts_nn.MctsNN()
  elif name == 'MCTSNN':
    agent = mcts_nn.MctsNN(checkpoint_dir, '~/tensorboard/')
  elif name == 'LONGEST_ROAD':
    agent = BuildLongestRoadAgent()
  else:
    agent = RandomAgent()
  return agent



def main():
  parser = argparse.ArgumentParser(description='Compare ticket to ride agents.')
  parser.add_argument('--agent1', default='MCTS', type=str)
  parser.add_argument('--agent2', default='RANDOM', type=str)
  parser.add_argument('--games', default=100, type=int)
  parser.add_argument('--checkpoint_dir', default='~/checkpoint', type=str)
  parser.add_argument('--report', dest='savereport', action='store_true')
  parser.add_argument('--no-report', dest='savereport', action='store_false')
  parser.add_argument('--reportdir', default='~/report', type=str)
  args = parser.parse_args()
  print('%s %s' % (args.agent1, args.agent2))

  agent1 = get_agent_for_name(args.agent1, args.agent2, args.checkpoint_dir)
  agent2 = get_agent_for_name(args.agent2, args.agent1, args.checkpoint_dir)

  wins = [0, 0]
  scores_total = [0, 0]
  num_games = args.games
  start_time = time.time()
  for i in range(num_games):
    reportdir = None
    if args.savereport:
      reportdir = os.path.expanduser(os.path.join(args.reportdir, str(i)))
      try:
        os.makedirs(reportdir)
      except OSError:
        pass
    game = Game([agent1, agent2], args.agent1, args.agent2, reportdir)
    scores = game.run()

    for i in range(len(scores)):
      scores_total[i] += scores[i]
    if scores[0] > scores[1]:
      wins[0] += 1
    if scores[1] > scores[0]:
      wins[1] += 1
  end_time = time.time()
  print('Total time: %s' % str(end_time - start_time))

  print('Scores:')
  for player in range(len(scores_total)):
    scores_total[player] = float(scores_total[player]) / float(num_games)
    wins[player] = float(wins[player]) / float(num_games)

    score = scores_total[player]
    print('  %d: %s, wins = %s' % (player, str(score), str(wins[player])))


if __name__ == '__main__':
  main()
    
