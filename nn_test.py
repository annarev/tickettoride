import unittest
import game_setup
from game_setup import *
from model import VisibleState
import nn


class NNTest(unittest.TestCase):

  def testConvertVisibleStateToInput(self):
    board = game_setup.getDefaultBoard()

    # 8 possible card colors
    train_cards = [0, 0, 0, 8, 0, 0, 0, 9]

    destination_cards = [0]*len(game_setup.DESTINATION_CARDS)
    destination_cards[7] = 1

    route_state = [0]*len(board.roads)
    route_state[21] = 1
    route_state[32] = 2

    state = VisibleState(
        current_player=1,
        player_count=2,
        train_cards=train_cards,
        destination_cards=destination_cards,
        route_state=route_state,
        player_to_train_count=[5, 1],
        train_deck_size=50,
        last_round_counter=-1)
    encoded_graph_state = nn.encode_graph_state(board, state)
    self.assertEqual((99, 48), encoded_graph_state.shape)

    # 2nd road is of length 1 and we have enough trains for it.
    self.assertEqual(1, encoded_graph_state[1, 45])
    # 5th road is from Seattle to Calgary of length 4.
    # We don't have enough trains to build it.
    self.assertEqual(0, encoded_graph_state[4, 45])

  def testEdgeAdjacencyMatrix(self):
    board = game_setup.getDefaultBoard()
    adj = nn.edge_adjacency_matrix(board)

    roads1 = board.getParallelRoads(City.VANCOUVER, City.SEATTLE)   
    roads2 = board.getParallelRoads(City.PORTLAND, City.SEATTLE)   
    self.assertTrue(roads1)
    self.assertTrue(roads2)
    for r1 in roads1:
      for r2 in roads2:
        self.assertGreater(adj[r1.id][r2.id], 0)
        self.assertGreater(adj[r2.id][r1.id], 0)

    roads3 = board.getParallelRoads(City.TORONTO, City.DULUTH)   
    self.assertTrue(roads3)
    for r1 in roads1:
      for r3 in roads3:
        self.assertEqual(adj[r1.id][r3.id], 0)

    for r2 in roads2:
      for r3 in roads3:
        self.assertEqual(adj[r2.id][r3.id], 0)

  def testRoadColorsStaySame(self):
    expected_colors = '[<Color.WHITE: 1>, <Color.WHITE: 1>, <Color.ORANGE: 4>, <Color.WHITE: 1>, <Color.GREEN: 8>, <Color.WHITE: 1>, <Color.WHITE: 1>, <Color.BLUE: 5>, <Color.YELLOW: 3>, <Color.BLUE: 5>, <Color.ORANGE: 4>, <Color.BLACK: 2>, <Color.PINK: 7>, <Color.GREEN: 8>, <Color.RED: 6>, <Color.BLUE: 5>, <Color.GREEN: 8>, <Color.PINK: 7>, <Color.ORANGE: 4>, <Color.WHITE: 1>, <Color.YELLOW: 3>, <Color.PINK: 7>, <Color.YELLOW: 3>, <Color.ORANGE: 4>, <Color.YELLOW: 3>, <Color.RED: 6>, <Color.PINK: 7>, <Color.BLACK: 2>, <Color.WHITE: 1>, <Color.BLACK: 2>, <Color.YELLOW: 3>, <Color.YELLOW: 3>, <Color.BLACK: 2>, <Color.PINK: 7>, <Color.BLACK: 2>, <Color.ORANGE: 4>, <Color.RED: 6>, <Color.BLUE: 5>, <Color.YELLOW: 3>, <Color.RED: 6>, <Color.GREEN: 8>, <Color.WHITE: 1>, <Color.ORANGE: 4>, <Color.WHITE: 1>, <Color.BLUE: 5>, <Color.WHITE: 1>, <Color.YELLOW: 3>, <Color.ORANGE: 4>, <Color.GREEN: 8>, <Color.WHITE: 1>, <Color.BLACK: 2>, <Color.PINK: 7>, <Color.WHITE: 1>, <Color.PINK: 7>, <Color.RED: 6>, <Color.BLUE: 5>, <Color.BLUE: 5>, <Color.PINK: 7>, <Color.GREEN: 8>, <Color.BLUE: 5>, <Color.YELLOW: 3>, <Color.GREEN: 8>, <Color.PINK: 7>, <Color.GREEN: 8>, <Color.WHITE: 1>, <Color.WHITE: 1>, <Color.PINK: 7>, <Color.BLACK: 2>, <Color.YELLOW: 3>, <Color.ORANGE: 4>, <Color.BLACK: 2>, <Color.PINK: 7>, <Color.GREEN: 8>, <Color.BLACK: 2>, <Color.WHITE: 1>, <Color.YELLOW: 3>, <Color.ORANGE: 4>, <Color.BLACK: 2>, <Color.YELLOW: 3>, <Color.BLACK: 2>, <Color.BLUE: 5>, <Color.BLUE: 5>, <Color.PINK: 7>, <Color.RED: 6>, <Color.BLUE: 5>, <Color.YELLOW: 3>, <Color.PINK: 7>, <Color.ORANGE: 4>, <Color.PINK: 7>, <Color.BLUE: 5>, <Color.WHITE: 1>, <Color.GREEN: 8>, <Color.ORANGE: 4>, <Color.BLACK: 2>, <Color.YELLOW: 3>, <Color.RED: 6>, <Color.BLUE: 5>, <Color.RED: 6>, <Color.RED: 6>]'
    board = game_setup.getDefaultBoard()
    colors = [r.color for r in board.roads]
    self.assertEqual(expected_colors, str(colors))

if __name__ == '__main__':
  unittest.main()
