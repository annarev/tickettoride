import copy
import enum
import numpy as np
import random

from collections import defaultdict

from game_setup import *

def getWinner(state):
  """Returns (winner, tie) tuple for current state of the game."""
  scores = state.calculateScores()
  if scores.count(scores[0]) == len(scores):
    return -1, True  # tie
  return np.argmax(scores), False


class Deck(object):
  def __init__(self, deck, shuffle=False, rand=None):
    self.deck = deck
    if shuffle:
      if rand:
        rand.shuffle(self.deck)
      else:
        random.shuffle(self.deck)

  def size(self):
    return len(self.deck)

  def deepCopy(self):
    return Deck(self.deck)

  def canDrawCard(self):
    return len(self.deck) > 1

  def takeTopCard(self):
    if not self.deck:  # ran out of cards
      return None
    top_card = self.deck[-1]
    self.deck = self.deck[:-1]
    return top_card

  def takeTopNCards(self, n):
    raise UnimplementedError('Implemented in subclasses')


class DestinationDeck(Deck):
  def takeTopNCards(self, n):
    """Returns dictionary mapping card color to card"""
    top_n_cards = self.deck[-n:]
    self.deck = self.deck[:-n]
    return top_n_cards

class TrainDeck(Deck):
  def takeTopNCards(self, n):
    """Returns dictionary mapping card color to card"""
    top_n_cards = self.deck[-n:]
    self.deck = self.deck[:-n]

    color_to_count = [0]*8  # 8 colors (excluding gray)
    for card in top_n_cards:
      color_to_count[card] += 1
    return color_to_count

def getTrainDeck(rand=None):
  cards = []
  for c in range(COLOR_COUNT_WITH_GRAY):
    if c != COLOR_GRAY:
      cards += [c]*12
  return TrainDeck(cards, shuffle=True, rand=rand)


def getDestinationDeck(rand=None):
  return DestinationDeck(
          list(DESTINATION_CARDS), shuffle=True, rand=rand)


def initialState(board, player_count, start_player=None):
   # Init
   # Each player gets:
   #   3 destination cards
   #   4 train cards
   #   45 train cars
   #start_player = random.choice([0, 1]) if start_player is None else start_player
   start_player = 0
   train_deck = getTrainDeck() #rand=rand)
   destination_deck = getDestinationDeck() #rand=rand)

   # Dictionary mapping player to array of the form:
   # 0 - player doesn't have the card
   # 1 - player has the card
   player_to_destination_cards = {}
   # 0 if road not claimed
   # 1 if player 0 has the road
   # 2 if player 1 has the road
   # -1 if road is not available (if sibling road has been claimed)
   route_state = [0]*len(board.roads)
   player_to_card_count = defaultdict(list)
   player_to_train_count = [45, 45]
   initial_destinations = 3

   for player in range(player_count):
     player_to_card_count[player] = train_deck.takeTopNCards(4)
     player_to_destination_cards[player] = [
         card.id for card in destination_deck.takeTopNCards(initial_destinations)]

   discarded_cards = np.zeros((COLOR_COUNT,), np.int32)
   opponent_card_count = 4
   return State(
       board, player_count, start_player,
       opponent_card_count,
       train_deck, destination_deck,
       player_to_destination_cards,
       route_state,
       player_to_card_count,
       player_to_train_count,
       discarded_cards)

def GuessDestinationCards1(visible_state):
  player_id = visible_state.current_player
  opponent_id = 1 - player_id
  available_destination_cards = [
    d for d in list(DESTINATION_CARDS) if d.id not in visible_state.destination_cards]
  destination_deck = DestinationDeck(available_destination_cards, shuffle=True)
  opponent_destination_cards = [c.id for c in destination_deck.takeTopNCards(3)]
  player_to_destination_cards = {
    player_id: visible_state.destination_cards,
    opponent_id: opponent_destination_cards
  }
  return player_to_destination_cards, destination_deck

def GuessDestinationCards2(visible_state):
  player_id = visible_state.current_player
  opponent_id = 1 - player_id
  R = [visible_state.board.roads[i].distance
       if s == opponent_id + 1 else 0
       for i, s in enumerate(visible_state.route_state)]
  R = np.array(R, dtype=np.float32)
  E = getUnnormEdgeMatrix()
  gamma = 0.7
  I = np.identity(E.shape[0])
  S = R  # level 1 neighbor
  S += np.matmul(E - I, R) * gamma  # level 2 neighbor
  D = destination_matrix()
  dest_prob = np.matmul(D, S) + 0.1  # add a bit of probability for all cards
  dest_prob[visible_state.destination_cards] = 0
  dest_prob = dest_prob / np.sum(dest_prob)
  opponent_destination_cards = np.random.choice(
      range(len(dest_prob)), size=3, replace=False, p=dest_prob)
  player_to_destination_cards = {
    player_id: visible_state.destination_cards,
    opponent_id: list(opponent_destination_cards)
  }
  available_destination_cards = [
    d for d in list(DESTINATION_CARDS) if d.id not in visible_state.destination_cards
    and d.id not in opponent_destination_cards]
  destination_deck = DestinationDeck(available_destination_cards, shuffle=True)
  return player_to_destination_cards, destination_deck

def GuessStateFromVisibleState(visible_state):
  player_id = visible_state.current_player
  opponent_id = 1 - player_id
  # Color cards locations: player0, player1, discard pile
  # Available cards = total cards - currentplayer cards - discard pile cards
  available_card_counts = np.full((COLOR_COUNT,), 12)  # 12 cards of each color
  available_card_counts -= np.array(visible_state.train_cards, np.int32) + visible_state.discarded_cards
  available_cards = np.repeat(np.arange(available_card_counts.size), available_card_counts)
  opponent_train_cards = np.random.choice(
      available_cards, visible_state.opponent_card_count, replace=False)
  opponent_train_card_counts = np.bincount(opponent_train_cards, minlength=COLOR_COUNT)
  player_to_card_count = {
      player_id: visible_state.train_cards,
      opponent_id: opponent_train_card_counts
  }
  deck_card_counts = available_card_counts - opponent_train_card_counts
  train_deck_cards = np.repeat(np.arange(deck_card_counts.size), deck_card_counts)
  train_deck = TrainDeck(list(train_deck_cards), shuffle=True)

  player_to_destination_cards, destination_deck = GuessDestinationCards1(visible_state)
  state = State(
      visible_state.board,
      visible_state.player_count,
      visible_state.current_player,
      visible_state.opponent_card_count,
      train_deck, destination_deck,
      player_to_destination_cards,
      visible_state.route_state,
      player_to_card_count,
      visible_state.player_to_train_count,
      visible_state.discarded_cards)
  return state



class VisibleState(object):
  def __init__(
      self, board, current_player, player_count,
      opponent_card_count,
      train_cards, destination_cards, route_state,
      player_to_train_count, train_deck_size, last_round_counter,
      discarded_cards):
    self.board = board
    self.current_player = current_player
    self.player_count = player_count
    self.opponent_card_count = opponent_card_count
    self.train_cards = train_cards
    self.destination_cards = destination_cards
    self.route_state = route_state
    self.player_to_train_count = player_to_train_count
    self.train_deck_size = train_deck_size
    self.last_round_counter = last_round_counter
    self.discarded_cards = discarded_cards

  def __eq__(self, other):
    """
    Allows two states to be compared.
    """
    return (self.current_player == other.current_player and
            self.train_deck_size == other.train_deck_size and
            self.train_cards == other.train_cards and
            self.destination_cards == other.destination_cards and
            self.route_state == other.route_state and
            self.player_to_train_count == other.player_to_train_count)

  def __hash__(self):
    """
    Allows states to be keys of dictionaries.
    """
    #return hash(self.__str__())
    # Create a tuple of values:
    # 0: player id
    # 1-9: count of each card color
    # : destination card ids for current player (3 destination cards)
    # ?: -1, 0, 1, 2 for each road to indicate available, claimed by player 0, claimed by player 1
    # : plastic train count for each player

    # hash_array = [self.current_player, self.train_deck_size]
    # hash_array.extend(self.train_cards)
    # hash_array.extend(self.destination_cards)
    # hash_array.extend(self.route_state)
    # hash_array.extend(self.player_to_train_count)
    # return hash(tuple(hash_array))
    return hash((
      self.current_player,
      self.train_deck_size,
      hash(tuple(self.train_cards)),
      hash(tuple(self.destination_cards)),
      hash(tuple(self.route_state)),
      hash(tuple(self.player_to_train_count)),
      #hash(tuple(self.discarded_cards))
    ))

  def __str__(self):
    player_strings = [''] * self.player_count
    for player in range(self.player_count):
      player_strings[player] = """
Player: %d
Train count: %d
Train deck size: %d
""" % (player,
       self.player_to_train_count[player],
       self.train_deck_size)

      if self.current_player == player:
        player_train_cards = ', '.join(
            sorted(['%d: %d' % (color, count)
             for color, count in enumerate(self.train_cards)]))
        player_destination_cards = ', '.join(
            sorted([str(d) for d in self.destination_cards]))
        player_strings[player] += """Train cards: %s
Destination cards: %s
""" % (player_train_cards, player_destination_cards)

    return """Current player: %d
Routes: %s
Discarded cards: %s
  %s
""" % (self.current_player, str(self.route_state), str(self.discarded_cards),
       '\n'.join(player_strings))

     
     #    return """Current player: %d
     # Last round counter: %d
     # %s
     #""" % (self.current_player, self.last_round_counter, '\n'.join(player_strings))



class State(object):
  def __init__(
      self, board, player_count, current_player, opponent_card_count,
      train_deck, destination_deck,
      player_to_destination_cards,
      route_state,
      player_to_card_count,
      player_to_train_count,
      discarded_cards):

    # Static state
    self.board = board
    self.player_count = player_count

    # Dynamic state
    self.current_player = current_player
    self.opponent_card_count = opponent_card_count
    self.train_deck = train_deck
    self.destination_deck = destination_deck
    self.player_to_destination_cards = player_to_destination_cards
    self.route_state = route_state
    self.player_to_card_count = player_to_card_count
    self.player_to_train_count = player_to_train_count
    self.discarded_cards = discarded_cards

    # When a player has 0, 1, or 2 trains left at the end of turn,
    # then we start this counter by setting it to 0 and increment
    # it for each next turn.
    # When all players had a final turn
    # (i.e. when this counter is equal to player_count)_, game ends.
    self.last_round_counter = -1

  def __eq__(self, other):
    """
    Allows two states to be compared.
    """
    raise ValueError('Calling State __eq__')
    return self.__str__() == other.__str__()

  def __hash__(self):
    """
    Allows states to be keys of dictionaries.
    """
    return hash(self.__str__())

  def __str__(self):
    player_strings = [''] * self.player_count
    for player in range(self.player_count):
      player_train_cards = ', '.join(
          sorted(['%s: %d' % (str(color), count)
           for color, count in enumerate(self.player_to_card_count[player])]))
      player_destination_cards = ', '.join(
          sorted([str(d) for d in self.player_to_destination_cards[player]]))
      player_strings[player] = """
Player: %d
Destination cards: %s
Card count: %s
Train count: %d
Train deck size: %d
""" % (player, player_destination_cards,
       player_train_cards,
       self.player_to_train_count[player],
       len(self.train_deck.deck))
      
    return """Current player: %d
Road state: %s
Discarded cards: %s
  %s
""" % (self.current_player, str(self.route_state), str(self.discarded_cards),
       '\n'.join(player_strings))

  def deepCopy(self):
    #new_player_to_card_count = defaultdict(int, {
    #  player: copy.copy(card_counts) for player, card_counts in self.player_to_card_count.items()})
    new_player_to_card_count = {0: list(self.player_to_card_count[0]),
                                1: list(self.player_to_card_count[1])}
    new_state = State(
        self.board, self.player_count, self.current_player,
        self.opponent_card_count,
        self.train_deck.deepCopy(), self.destination_deck,
        self.player_to_destination_cards,
        list(self.route_state),
        new_player_to_card_count,
        copy.copy(self.player_to_train_count),
        self.discarded_cards.copy())
    new_state.last_round_counter = self.last_round_counter
    return new_state

  def getLegalActions(self):
    if self.isEnd():
      return []

    actions = []

    # Player can do one if 2 things: draw card, claim route
    # TODO: also support draw destination ticket
    if self.train_deck.canDrawCard():
      actions.append(self.board.getAction(Action(ActionType.DRAW_TRAIN_CARD)))

    available_cards = self.player_to_card_count[self.current_player]
    available_trains = self.player_to_train_count[self.current_player]
    max_cards = max(available_cards)

    roads = self.getAvailableRoads()
    available_routes = [
         r for r in roads
         if (r.distance <= available_trains and
          r.distance <= max_cards and
          (r.color == COLOR_GRAY or r.distance <= available_cards[r.color]))]

    for road in available_routes:
      if road.color == COLOR_GRAY:
        for color, count in enumerate(available_cards):
          if count >= road.distance:
            action = self.board.getAction(ClaimRoadAction(ActionType.CLAIM_ROUTE, road, color))
            actions.append(action)
      else:
        action = self.board.getAction(ClaimRoadAction(ActionType.CLAIM_ROUTE, road))
        actions.append(action)

    return actions

  def generateSuccessor(self, action, copy_state=True):
    state = None
    if not action:
      state = self.deepCopy() if copy_state else self
    elif action.action_type == ActionType.DRAW_TRAIN_CARD:
      state = self.succAfterDrawingCard(copy_state=copy_state)
    elif action.action_type == ActionType.CLAIM_ROUTE:
      state = self.succAfterClaimingRoad(action, copy_state=copy_state)

    # We should be able to tell how many cards opponent has
    # if we count each time opponent takes cards from pile.
    state.opponent_card_count = sum(state.player_to_card_count[self.current_player])
    state.advancePlayer()
    if state.last_round_counter >= 0:
      state.last_round_counter += 1

    #assert state != None
    return state

  def succAfterDrawingCard(self, copy_state=True):
    new_state = self.deepCopy() if copy_state else self
    top_card1 = new_state.train_deck.takeTopCard()
    top_card2 = new_state.train_deck.takeTopCard()
    new_state.player_to_card_count[self.current_player][top_card1] += 1 
    new_state.player_to_card_count[self.current_player][top_card2] += 1 
    return new_state

  def succAfterClaimingRoad(self, claim_road_action, copy_state=True):
    road = claim_road_action.road
    color = road.color
    if claim_road_action.color is not None:
      color = claim_road_action.color

    new_state = self.deepCopy() if copy_state else self

    assert new_state.route_state[road.id] == 0

    new_state.route_state[road.id] = self.current_player + 1
    if road.sibling:
      assert new_state.route_state[road.sibling.id] == 0
      new_state.route_state[road.sibling.id] = -1  # sibling road no longer available
    
    new_state.player_to_card_count[self.current_player][color] -= road.distance
    new_state.player_to_train_count[self.current_player] -= road.distance
    new_state.discarded_cards[color] += road.distance
    #assert new_state.player_to_card_count[self.current_player][color.value-1] >= 0
    assert new_state.player_to_train_count[self.current_player] >= 0
    
    trains_left = new_state.player_to_train_count[self.current_player]
    if trains_left <= 2:
      new_state.last_round_counter = 0
    return new_state

  def getAvailableRoads(self):
    roads = [self.board.roads[i] for i, status in enumerate(self.route_state)
             if status == 0]
    return roads

  def advancePlayer(self):
    self.current_player = (self.current_player + 1) % self.player_count

  def calculateScore(self, player, player_roads):

    # Add up route length points
    road_length_points = 0
    for route in player_roads:
      road_length_points += ROUTE_LENGTH_SCORES[route.distance]

    # Add up destination route points
    destination_points = 0
    for destination_id in self.player_to_destination_cards[player]:
      destination = DESTINATION_CARDS[destination_id]
      if destinationConnected(destination, player_roads):
        destination_points += destination.reward
      else:
        destination_points -= destination.reward

    return road_length_points + destination_points

  def calculateScores(self):
    player_roads = {}
    for player in range(self.player_count):
      player_roads[player] = [self.board.roads[i] for i, s in enumerate(self.route_state)
                              if s == player+1]
    player_scores = [self.calculateScore(p, player_roads[p]) for p in range(self.player_count)]

    player_with_longest_path = -1
    longest_path_length = -1
    longest_paths_are_same = True

    for player in range(self.player_count):
      length = longestPathLength(player_roads[player])
      if longest_path_length != -1 and length != longest_path_length:
        longest_paths_are_same = False
      if length > longest_path_length:
        longest_path_length = length
        player_with_longest_path = player

    if longest_paths_are_same:
      return [s + 10 for s in player_scores]

    assert player_with_longest_path >= 0 
    # assert longest_path_length >= 1
    player_scores[player_with_longest_path] += 10
    return player_scores
  

  def isEnd(self):
    # Game ends when all players got a turn in the last round.  
    return self.last_round_counter == self.player_count
  

  def getVisibleState(self, player=None):
    if player is None:
      player = self.current_player
    return VisibleState(
        self.board,
        self.current_player,
        self.player_count,
        self.opponent_card_count,
        self.player_to_card_count[player],
        self.player_to_destination_cards[player],
        self.route_state,
        self.player_to_train_count,
        self.train_deck.size(),
        self.last_round_counter,
        self.discarded_cards)


