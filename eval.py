import data
import nn
import os
import nn
from game_setup import *

checkpoint_dir = os.path.expanduser('~/checkpointmix/')
tensorboard_dir = os.path.expanduser('~/tensorboardmix/')
datadir = os.path.expanduser('~/datasetmix')

board = getDefaultBoard()
d = data.load_multiple_examples(os.path.join(datadir, 'data'))
n = nn.TicketToRideNN(board, checkpoint_dir, tensorboard_dir)

n.eval(d)

