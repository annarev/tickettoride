import enum
from collections import defaultdict
import numpy as np
import time
import random

CITY_VANCOUVER = 0
CITY_CALGARY = 1
CITY_WINNIPEG = 2
CITY_SAULT_STE_MARIE = 3
CITY_MONTREAL = 4
CITY_TORONTO = 5
CITY_SEATTLE = 6
CITY_PORTLAND = 7
CITY_HELENA = 8
CITY_DULUTH = 9
CITY_CHICAGO = 10
CITY_PITTSBURGH = 11
CITY_NEW_YORK = 12
CITY_SALT_LAKE_CITY = 13
CITY_DENVER = 14
CITY_OMAHA = 15
CITY_KANSAS_CITY = 16
CITY_SAINT_LOUIS = 17
CITY_NASHVILLE = 18
CITY_WASHINGTON = 19
CITY_RALEIGH = 20
CITY_OKLAHOMA_CITY = 21
CITY_SAN_FRANCISCO = 22
CITY_LAS_VEGAS = 23
CITY_SANTA_FE = 24
CITY_LITTLE_ROCK = 25
CITY_ATLANTA = 26
CITY_CHARLESTON = 27
CITY_LOS_ANGELES = 28
CITY_PHOENIX = 29
CITY_EL_PASO = 30
CITY_DALLAS = 31
CITY_HOUSTON = 32
CITY_NEW_ORLEANS = 33
CITY_MIAMI = 34
CITY_BOSTON = 35
CITY_COUNT = 36

COLOR_WHITE = 0
COLOR_BLACK = 1
COLOR_YELLOW = 2
COLOR_ORANGE = 3
COLOR_BLUE = 4
COLOR_RED = 5
COLOR_PINK = 6
COLOR_GREEN = 7
COLOR_COUNT = 8
COLOR_GRAY = 8
COLOR_COUNT_WITH_GRAY = 9

class Road(object):
  def __init__(self, color, distance, city1, city2):
    # TODO: begin remove
    if color ==COLOR_GRAY:
      rand = random.Random(city1 + city2)
      color = rand.choice(range(COLOR_COUNT))
    # TODO: end remove
    self.color = color
    self.distance = distance
    self.city1 = city1
    self.city2 = city2
    # We set sibling if we have 2 roads in parallel between 2 cities.
    # Sibling would point to the road next to current road.
    self.sibling = None
    self.id = 0

  def add_sibling(self, road):
    assert self.sibling == None
    self.sibling = road

  def get_other_endpoint(self, city):
    assert city == self.city1 or city == self.city2
    return self.city2 if city == self.city1 else self.city1

  def __str__(self):
    return '(%s, %s, %d, %s)' % (
        self.city1, self.city2, self.distance, str(self.color))

  def __hash__(self):
    return self.id
  
  def _to_tuple(self):
    return (self.city1, self.city2, self.distance, self.color)

  def __eq__(self, other):
    return self.id == other.id


class ActionType(enum.Enum):
  DRAW_TRAIN_CARD = 0
  CLAIM_ROUTE = 1
  # Add DRAW_DESTINATION_CARD

class Action(object):
  def __init__(self, action_type):
    self.action_type = action_type
    self.id = 0

  def action_identifier(self):
    return (self.action_type,)

  def __hash__(self):
    return self.id

  def __str__(self):
    return str(self.action_type)

  def __eq__(self, other):
    return self.id == other.id
 

class ClaimRoadAction(Action):
  def __init__(self, action_type, route, color=None, id_=0):
    Action.__init__(self, action_type)
    self.road = route
    self.color = color
    if color == None:
      self.color = route.color
    self.id = id_

  def action_identifier(self):
    return (self.action_type, self.road, self.color)

  def __str__(self):
    return '%s_%s_%s' % (
      str(self.action_type),
      str(self.road),
      str(self.color))


# Create a pre-defined list of actions
def getAllActions(board):
  actions = [Action(ActionType.DRAW_TRAIN_CARD)]
  for road in board.roads:
    if road.color == COLOR_GRAY:
      # Add an action for every color
      for color in range(COLOR_COUNT_WITH_GRAY):
        if color == COLOR_GRAY: continue
        actions.append(ClaimRoadAction(ActionType.CLAIM_ROUTE, road, color, len(actions)))
    else:  # non-gray road
      actions.append(ClaimRoadAction(ActionType.CLAIM_ROUTE, road, road.color, len(actions)))
  return actions
    

class Board(object):
  def __init__(self):
    self.roads = []
    self.city_to_roads = defaultdict(list)
    self.actions = []  # All possible actions, independent of state
    self.action_to_id = {}

  def add_road(self, city1, city2, distance, color, additional_color=-1):
    road = Road(color, distance, city1, city2)
    road.id = len(self.roads)
    self.roads.append(road)
    self.city_to_roads[road.city1].append(road)
    self.city_to_roads[road.city2].append(road)

    # sibling road
    if additional_color != -1:
      road2 = Road(additional_color, distance, city1, city2)
      road2.id = len(self.roads)
      self.roads.append(road2)
      self.city_to_roads[road2.city1].append(road2)
      self.city_to_roads[road2.city2].append(road2)
      road.add_sibling(road2)
      road2.add_sibling(road)

  def initActions(self):
    self.actions = getAllActions(self)
    for i, action in enumerate(self.actions):
      action.id = i
      self.action_to_id[action.action_identifier()] = i

  def initDestinations(self):
    for i, destination_card in enumerate(DESTINATION_CARDS):
      destination_card.id = i

  def getAction(self, action):
    # Get action with id set
    return self.actions[self.action_to_id[action.action_identifier()]]

  def getActionById(self, action_id):
    return self.actions[action_id]

  def numActions(self):
    return len(self.actions)

  def getRoads(self, city):
    return self.city_to_roads[city]

  def getParallelRoads(self, city1, city2):
    city1_roads = set(self.city_to_roads[city1])
    city2_roads = set(self.city_to_roads[city2])
    connecting_roads = city1_roads.intersection(city2_roads)
    return connecting_roads

  def __iter__(self):
    return self.roads.__iter__()
    

def initDefaultBoard():
  board = Board()
  board.add_road(CITY_VANCOUVER, CITY_SEATTLE, 1,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_PORTLAND, CITY_SEATTLE, 1,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_SEATTLE, CITY_CALGARY, 4,COLOR_GRAY)
  board.add_road(CITY_VANCOUVER, CITY_CALGARY, 3,COLOR_GRAY)
  board.add_road(CITY_WINNIPEG, CITY_CALGARY, 6,COLOR_WHITE)
  board.add_road(CITY_HELENA, CITY_CALGARY, 4,COLOR_GRAY)
  board.add_road(CITY_HELENA, CITY_SEATTLE, 6,COLOR_YELLOW)
  board.add_road(CITY_HELENA, CITY_WINNIPEG, 4,COLOR_BLUE)
  board.add_road(CITY_HELENA, CITY_DULUTH, 6,COLOR_ORANGE)
  board.add_road(CITY_WINNIPEG, CITY_DULUTH, 4,COLOR_BLACK)
  board.add_road(CITY_HELENA, CITY_SALT_LAKE_CITY, 3,COLOR_PINK)
  board.add_road(CITY_HELENA, CITY_DENVER, 4,COLOR_GREEN)
  board.add_road(CITY_HELENA, CITY_OMAHA, 5,COLOR_RED)
  board.add_road(CITY_PORTLAND, CITY_SALT_LAKE_CITY, 6,COLOR_BLUE)
  board.add_road(CITY_PORTLAND, CITY_SAN_FRANCISCO, 5,COLOR_GREEN,COLOR_PINK)
  board.add_road(CITY_SALT_LAKE_CITY, CITY_SAN_FRANCISCO, 5,COLOR_ORANGE,COLOR_WHITE)
  board.add_road(CITY_LOS_ANGELES, CITY_SAN_FRANCISCO, 3,COLOR_YELLOW,COLOR_PINK)
  board.add_road(CITY_LOS_ANGELES, CITY_LAS_VEGAS, 2,COLOR_GRAY)
  board.add_road(CITY_LAS_VEGAS, CITY_SALT_LAKE_CITY, 3,COLOR_ORANGE)
  board.add_road(CITY_SALT_LAKE_CITY, CITY_DENVER, 3,COLOR_YELLOW,COLOR_RED)
  board.add_road(CITY_LOS_ANGELES, CITY_PHOENIX, 3,COLOR_GRAY)
  board.add_road(CITY_LOS_ANGELES, CITY_EL_PASO, 6,COLOR_BLACK)
  board.add_road(CITY_PHOENIX, CITY_DENVER, 5,COLOR_WHITE)
  board.add_road(CITY_PHOENIX, CITY_SANTA_FE, 3,COLOR_GRAY)
  board.add_road(CITY_PHOENIX, CITY_EL_PASO, 3,COLOR_GRAY)
  board.add_road(CITY_SANTA_FE, CITY_EL_PASO, 2,COLOR_GRAY)
  board.add_road(CITY_DENVER, CITY_SANTA_FE, 2,COLOR_GRAY)
  board.add_road(CITY_DENVER, CITY_OMAHA, 4,COLOR_PINK)
  board.add_road(CITY_DENVER, CITY_KANSAS_CITY, 4,COLOR_BLACK,COLOR_ORANGE)
  board.add_road(CITY_DENVER, CITY_OKLAHOMA_CITY, 4,COLOR_RED)
  board.add_road(CITY_SANTA_FE, CITY_OKLAHOMA_CITY, 3,COLOR_BLUE)
  board.add_road(CITY_EL_PASO, CITY_OKLAHOMA_CITY, 5,COLOR_YELLOW)
  board.add_road(CITY_EL_PASO, CITY_DALLAS, 4,COLOR_RED)
  board.add_road(CITY_EL_PASO, CITY_HOUSTON, 6,COLOR_GREEN)
  board.add_road(CITY_DALLAS, CITY_HOUSTON, 1,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_DALLAS, CITY_OKLAHOMA_CITY, 2,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_OKLAHOMA_CITY, CITY_KANSAS_CITY, 2,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_OMAHA, CITY_KANSAS_CITY, 1,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_OMAHA, CITY_DULUTH, 2,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_WINNIPEG, CITY_SAULT_STE_MARIE, 6,COLOR_GRAY)
  board.add_road(CITY_DULUTH, CITY_SAULT_STE_MARIE, 3,COLOR_GRAY)
  board.add_road(CITY_DULUTH, CITY_TORONTO, 6,COLOR_PINK)
  board.add_road(CITY_DULUTH, CITY_CHICAGO, 3,COLOR_RED)
  board.add_road(CITY_OMAHA, CITY_CHICAGO, 4,COLOR_BLUE)
  board.add_road(CITY_KANSAS_CITY, CITY_SAINT_LOUIS, 2,COLOR_BLUE,COLOR_PINK)
  board.add_road(CITY_OKLAHOMA_CITY, CITY_LITTLE_ROCK, 2,COLOR_GRAY)
  board.add_road(CITY_DALLAS, CITY_LITTLE_ROCK, 2,COLOR_GRAY)
  board.add_road(CITY_HOUSTON, CITY_NEW_ORLEANS, 2,COLOR_GRAY)
  board.add_road(CITY_NEW_ORLEANS, CITY_LITTLE_ROCK, 3,COLOR_GREEN)
  board.add_road(CITY_LITTLE_ROCK, CITY_SAINT_LOUIS, 2,COLOR_GRAY)
  board.add_road(CITY_CHICAGO, CITY_SAINT_LOUIS, 2,COLOR_GREEN,COLOR_WHITE)
  board.add_road(CITY_CHICAGO, CITY_TORONTO, 2,COLOR_WHITE)
  board.add_road(CITY_SAULT_STE_MARIE, CITY_TORONTO, 2,COLOR_GRAY)
  board.add_road(CITY_SAULT_STE_MARIE, CITY_MONTREAL, 5,COLOR_BLACK)
  board.add_road(CITY_TORONTO, CITY_MONTREAL, 3,COLOR_GRAY)
  board.add_road(CITY_CHICAGO, CITY_PITTSBURGH, 3,COLOR_ORANGE,COLOR_BLACK)
  board.add_road(CITY_PITTSBURGH, CITY_TORONTO, 2,COLOR_GRAY)
  board.add_road(CITY_SAINT_LOUIS, CITY_PITTSBURGH, 5,COLOR_GREEN)
  board.add_road(CITY_SAINT_LOUIS, CITY_NASHVILLE, 2,COLOR_GRAY)
  board.add_road(CITY_LITTLE_ROCK, CITY_NASHVILLE, 3,COLOR_WHITE)
  board.add_road(CITY_NEW_ORLEANS, CITY_ATLANTA, 4,COLOR_YELLOW,COLOR_ORANGE)
  board.add_road(CITY_NASHVILLE, CITY_ATLANTA, 1,COLOR_BLACK)
  board.add_road(CITY_NASHVILLE, CITY_PITTSBURGH, 4,COLOR_YELLOW)
  board.add_road(CITY_NASHVILLE, CITY_RALEIGH, 3,COLOR_BLACK)
  board.add_road(CITY_ATLANTA, CITY_RALEIGH, 2,COLOR_GRAY,COLOR_GRAY)
  board.add_road(CITY_RALEIGH, CITY_PITTSBURGH, 2,COLOR_GRAY)
  board.add_road(CITY_NEW_ORLEANS, CITY_MIAMI, 6,COLOR_RED)
  board.add_road(CITY_MIAMI, CITY_ATLANTA, 5,COLOR_BLUE)
  board.add_road(CITY_ATLANTA, CITY_CHARLESTON, 2,COLOR_GRAY)
  board.add_road(CITY_MIAMI, CITY_CHARLESTON, 4,COLOR_PINK)
  board.add_road(CITY_CHARLESTON, CITY_RALEIGH, 2,COLOR_GRAY)
  board.add_road(CITY_RALEIGH, CITY_WASHINGTON, 2,COLOR_GRAY)
  board.add_road(CITY_PITTSBURGH, CITY_WASHINGTON, 2,COLOR_GRAY)
  board.add_road(CITY_PITTSBURGH, CITY_NEW_YORK, 2,COLOR_WHITE,COLOR_GREEN)
  board.add_road(CITY_WASHINGTON, CITY_NEW_YORK, 2,COLOR_ORANGE,COLOR_BLACK)
  board.add_road(CITY_NEW_YORK, CITY_BOSTON, 2,COLOR_YELLOW,COLOR_RED)
  board.add_road(CITY_MONTREAL, CITY_NEW_YORK, 3,COLOR_BLUE)
  board.add_road(CITY_MONTREAL, CITY_BOSTON, 2,COLOR_GRAY,COLOR_GRAY)

  board.initActions()
  board.initDestinations()
  print('Num routes: %d' % len(board.roads))
  return board


_default_board = None 
def getDefaultBoard():
  global _default_board
  if not _default_board:
    _default_board = initDefaultBoard()
  return _default_board

def edge_adjacency_matrix(board, normalize=True):
  edge_count = len(board.roads)
  edge_adjacency_matrix = np.zeros((edge_count, edge_count), dtype=np.float32)

  # Set 1 for each pair of connected roads
  for city in range(CITY_COUNT):
    roads = board.getRoads(city)
    for road1 in roads:
      for road2 in roads:
        edge_adjacency_matrix[road1.id][road2.id] = 1

  if normalize:
    degree_matrix = np.diag(np.sum(edge_adjacency_matrix, 1))
    degree_matrix_inv = np.linalg.inv(np.sqrt(degree_matrix))
    edge_adjacency_matrix = np.matmul(degree_matrix_inv, np.matmul(edge_adjacency_matrix, degree_matrix_inv))
  return edge_adjacency_matrix

_adjacency_matrix = None
def getEdgeMatrix():
  global _default_board
  global _adjacency_matrix
  if _adjacency_matrix is None:
    _adjacency_matrix = edge_adjacency_matrix(_default_board)
  return _adjacency_matrix

_unnorm_adjacency_matrix = None
def getUnnormEdgeMatrix():
  global _default_board
  global _unnorm_adjacency_matrix
  if _unnorm_adjacency_matrix is None:
    _unnorm_adjacency_matrix = edge_adjacency_matrix(_default_board, normalize=False)
  return _unnorm_adjacency_matrix

_destination_matrix = None
def destination_matrix():
  global _default_board
  global _destination_matrix
  if _destination_matrix is None:
    num_roads = len(_default_board.roads)
    _destination_matrix = np.zeros((len(DESTINATION_CARDS), num_roads), np.float32)
    for di, d in enumerate(DESTINATION_CARDS):
      for ri, r in enumerate(_default_board.roads):
        if (d.city1 == r.city1 or
            d.city2 == r.city2 or
            d.city1 == r.city2 or
            d.city2 == r.city1):
          _destination_matrix[di,ri] = 1
  return _destination_matrix

class DestinationCard(object):
  def __init__(self, city1, city2, reward):
    self.city1 = city1
    self.city2 = city2
    self.reward = reward
    self.id = 0

  def __str__(self):
    return '(%s, %s, %d)' % (self.city1, self.city2, self.reward)

  def __eq__(self, other):
    return self.id == other.id



DESTINATION_CARDS = [
  DestinationCard(CITY_LOS_ANGELES, CITY_NEW_YORK, 21),
  DestinationCard(CITY_DULUTH, CITY_HOUSTON, 8),
  DestinationCard(CITY_SAULT_STE_MARIE, CITY_NASHVILLE, 8),
  DestinationCard(CITY_NEW_YORK, CITY_ATLANTA, 6),
  DestinationCard(CITY_PORTLAND, CITY_NASHVILLE, 17),
  DestinationCard(CITY_VANCOUVER, CITY_MONTREAL, 20),
  DestinationCard(CITY_DULUTH, CITY_EL_PASO, 10),
  DestinationCard(CITY_TORONTO, CITY_MIAMI, 10),
  DestinationCard(CITY_PORTLAND, CITY_PHOENIX, 11),
  DestinationCard(CITY_DALLAS, CITY_NEW_YORK, 11),
  DestinationCard(CITY_CALGARY, CITY_SALT_LAKE_CITY, 7),
  DestinationCard(CITY_CALGARY, CITY_PHOENIX, 13),
  DestinationCard(CITY_WINNIPEG, CITY_LITTLE_ROCK, 11),
  DestinationCard(CITY_SAN_FRANCISCO, CITY_ATLANTA, 17),
  DestinationCard(CITY_KANSAS_CITY, CITY_HOUSTON, 5),
  DestinationCard(CITY_LOS_ANGELES, CITY_CHICAGO, 16),
  DestinationCard(CITY_CHICAGO, CITY_SANTA_FE, 9),
  DestinationCard(CITY_VANCOUVER, CITY_SANTA_FE, 13),
  DestinationCard(CITY_CHICAGO, CITY_NEW_ORLEANS, 7),
  DestinationCard(CITY_MONTREAL, CITY_ATLANTA, 9),
  DestinationCard(CITY_SEATTLE, CITY_NEW_YORK, 22),
  DestinationCard(CITY_DENVER, CITY_EL_PASO, 4),
  DestinationCard(CITY_HELENA, CITY_LOS_ANGELES, 8),
  DestinationCard(CITY_MONTREAL, CITY_NEW_ORLEANS, 13),
  DestinationCard(CITY_WINNIPEG, CITY_HOUSTON, 12),
  DestinationCard(CITY_SAULT_STE_MARIE, CITY_OKLAHOMA_CITY, 9),
  DestinationCard(CITY_SEATTLE, CITY_LOS_ANGELES, 9),
  DestinationCard(CITY_DENVER, CITY_PITTSBURGH, 11),
  DestinationCard(CITY_LOS_ANGELES, CITY_MIAMI, 20),
  DestinationCard(CITY_BOSTON, CITY_MIAMI, 12),
]

ROUTE_LENGTH_SCORES = {1:1, 2:2, 3:4, 4:7, 5:10, 6:15}

def longestPathLengthRecursive(city, routes_dict, visited): 
  if city not in routes_dict:
    return 0
  max_path_length = 0
  for neighbor_city, distance in routes_dict[city]:
    if neighbor_city not in routes_dict:
      continue
    if (neighbor_city, city) in visited:
      continue
    path_length = distance + longestPathLengthRecursive(
        neighbor_city, routes_dict,
        visited.union({(city, neighbor_city), (neighbor_city, city)}))
    if path_length > max_path_length:
      max_path_length = path_length
  return max_path_length

#longest_path_length_cache = {}

def longestPathLength(routes):
  """Get length of the longest connected route."""
  # global longest_path_length_cache
  # routes_id = tuple(sorted(r.id for r in routes))
  # if routes_id in longest_path_length_cache:
  #   return longest_path_length_cache[routes_id]
  routes_dict = defaultdict(list)
  for route in routes:
    routes_dict[route.city1].append((route.city2, route.distance))
    routes_dict[route.city2].append((route.city1, route.distance))

  lengths = [longestPathLengthRecursive(city, routes_dict, set())
             for city in routes_dict.keys()]
  max_length = max(lengths) if lengths else 0

  # Clear once in a while to avoid running out of memory
  # if len(longest_path_length_cache) > 20000:
  #   longest_path_length_cache = {}
  # longest_path_length_cache[routes_id] = max_length
  return max_length
    

def destinationConnected(destination, routes):
  """Check if destination is connected using routes."""
  routes_dict = defaultdict(list)
  for route in routes:
    routes_dict[route.city1].append(route.city2)
    routes_dict[route.city2].append(route.city1)

  stack = [destination.city1]
  visited = set()
  while stack:
    city = stack.pop()
    if city not in routes_dict:
      continue
    if city in visited:
      continue
    visited.add(city)
    for next_city in routes_dict[city]:
      if next_city == destination.city2:
        return True
      stack.append(next_city)
  return False

if __name__ == "__main__":
  board = getDefaultBoard()
  print(board.roads[48])
  print(board.roads[31])
  print(board.roads[41])
