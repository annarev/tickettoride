from game_setup import *
from model import *
from nn import TicketToRideNN
import agent
import argparse
import data
import collections
import examples
import gc
import math
import multiprocessing
import numpy as np
import os
import random
import time
import traceback


C_BASE = 19652
C_INIT = 1.25
#UCB_CONSTANT = 3
nn = None

def getAction(board, action_id):
  return board.getActionById(action_id)

def error(msg, *args):
  return multiprocessing.get_logger().error(msg, *args)


def runEpisodes(checkpoint_dir, tensorboard_dir, episode_count=1, mctsnn=None):
   policies = {}
   examples = []
   try:
     if mctsnn is None:
       mctsnn = MctsNN(checkpoint_dir, tensorboard_dir)
     for _ in range(episode_count):
       Q = defaultdict(float)
       N = defaultdict(float)
       player0_examples, player1_examples = mctsnn.executeEpisode(policies, Q, N)
       examples.append(player0_examples)
       examples.append(player1_examples)
   except Exception as e:
     error(traceback.format_exc())
     raise
   print('Collected %d examples' % len(examples))
   return examples


def train(checkpoint_dir,
          tensorboard_dir,
          num_iterations=1,
          num_workers=8,
          episodes=24,
          datadir=None):
  board = getDefaultBoard()
  if datadir is not None:
    #d = data.load_examples(os.path.join(datadir, 'data0'))
    d = data.load_multiple_examples(os.path.join(datadir, 'data'))
    nn = TicketToRideNN(board, checkpoint_dir, tensorboard_dir)
    nn.pretrain(d)
    return
  nn = TicketToRideNN(board, checkpoint_dir, tensorboard_dir)
  pool = multiprocessing.Pool(processes=num_workers)
  examples = []
  for i in range(num_iterations):
    policies = {}
    if len(examples) > 20000:
      examples = random.sample(examples, 20000)

    # execute episodes to get training data
    start_time = time.time()
    print('Running %d episodes across %d workers...' % (episodes, num_workers))

    episodes_per_worker = int(episodes / num_workers)
    #multiprocessing.log_to_stderr()
    #results = [pool.apply_async(runEpisodes, (checkpoint_dir, tensorboard_dir, episodes_per_worker))
    #           for _ in range(num_workers)]
    #for result in results:
    #  examples.extend(result.get(timeout=30*60))
    results = runEpisodes(checkpoint_dir, tensorboard_dir, episodes_per_worker)
    examples.extend(results)

    print('Running episodes took %d seconds' % (time.time() - start_time))
    print('Training with %d examples...' % len(examples))
    nn.train(examples)


class MctsNN(agent.Agent):
  def __init__(self, checkpoint_dir=None, tensorboard_dir=None, nn=None,
               cheating=False):
    self.board = getDefaultBoard()
    self.nn = nn
    if not nn:
      self.nn = TicketToRideNN(self.board, checkpoint_dir, tensorboard_dir)
    self.cheating = cheating

  def get_policy(self, visible_state, N):
    visit_counts = [N.get((visible_state, a), 0)
                    for a in range(self.board.numActions())]
    total_visits = float(sum(visit_counts))
    policy = [p / total_visits for p in visit_counts]
    return policy

  def executeEpisode(self, policies, Q, N):
    num_simulations = 128
    winner = None
    tie = False

    # separately keep track of examples for each player
    player0_examples = []
    player1_examples = []

    s = initialState(self.board, 2)  # initial state for 2 players
    legal_actions = s.getLegalActions()
    player0_examples = examples.Examples()
    player1_examples = examples.Examples()

    while True:
      visible_state = s.getVisibleState()
      for games in range(num_simulations):
        self.run_simulation(self.simulationState(s, visible_state), policies, Q, N)

      policy = self.get_policy(visible_state, N)
      if s.current_player == 0:
        player0_examples.add(visible_state, policy)
      else:  # current player is player 2
        player1_examples.add(visible_state, policy)

      legal_action_ids = [a.id for a in legal_actions]
      legal_policy = [policy[i] for i in legal_action_ids]
      a = np.random.choice(legal_actions, p=legal_policy)
      s = s.generateSuccessor(a)
      legal_actions = s.getLegalActions()
      if s.isEnd() or not legal_actions:
        winner, tie = getWinner(s)
        break

    assert tie or winner == 0 or winner == 1
    num_actions = self.board.numActions()
    player0_examples.update_outcomes(winner, tie, Q, N, num_actions)
    player1_examples.update_outcomes(winner, tie, Q, N, num_actions)
    return player0_examples.examples, player1_examples.examples

  def pickAction(self, state, legal_actions, return_policy=False):
    if not legal_actions:
      return None
    Q = defaultdict(float)
    N = defaultdict(float)
    #if len(legal_actions) == 1:
    #  if return_policy:
    #    policy = [0] * self.board.numActions()
    #    policy[legal_actions[0].id] = 1
    #    return legal_actions[0], policy, Q, N
    #  return legal_actions[0]

    player = state.current_player
    policies = {}
    visible_state = state.getVisibleState()

    # Get policy from NN
    #if self.nn.trained:
    #  policy, value = self.nn.predict(visible_state)
    #  print(value)
    #  #print('MCTSNN value: %s, current player: %d' % (str(value), state.current_player))
    #  wins_and_indexes = [(policy[a.id], i) for i, a in enumerate(legal_actions)]
    #  #print(wins_and_indexes)
    #percent_wins, action_index = max(wins_and_indexes)

    #if self.nn.trained:
    #  simulation_state = self.simulationState(state, visible_state)
    #  wins_and_indexes = []
    #  for i, action in enumerate(legal_actions):
    #    s = simulation_state.generateSuccessor(action)
    #    s = s.getVisibleState(player=player)
    #    s.current_player = player
    #    policy, value = self.nn.predict(s)
    #    wins_and_indexes.append((value, i))
    #percent_wins, action_index = max(wins_and_indexes)

    # Each time we might want to "guess" a different simulation state
    # from visible state.
    for t in range(5):
      simulation_state = self.simulationState(state, visible_state)
      for games in range(2):
        self.run_simulation(simulation_state, policies, Q, N)

    def reward(visible_state, action):
      state_key = (visible_state, action.id)
      q = Q.get(state_key, -1)
      q_weight = N.get(state_key, 1)
      return q / float(q_weight + 2)  # laplace smoothing
    wins_and_indexes = [(reward(visible_state, a), i) for i, a in enumerate(legal_actions)]
    percent_wins, action_index = max(wins_and_indexes)
    #print(wins_and_indexes)

    if return_policy:
      return legal_actions[action_index], self.get_policy(visible_state, N), Q, N
    return legal_actions[action_index]

  def pick_action_with_puct(self, state_id, actions, policy, Q, N):
    assert policy.shape == (self.board.numActions(),), "Policy shape: %s" % str(policy.shape)

    state_keys = [(state_id, a.id) for a in actions]

    q = [Q.get(s, -1) for s in state_keys]
    q_weight = [float(N.get(s, 0)) for s in state_keys]

    total_weight = sum(q_weight)
    if total_weight == 0:
      return random.choice(range(len(actions)))

    # Adjust policy
    # Policy for legal actions
    policy = [policy[a.id] for a in actions]
    # Put all probability weight on valid moves
    total_legal_policy = sum(policy)
    policy = [p / total_legal_policy for p in policy]
    # Add dirichlet noise
    alpha = [0.3] * len(policy)
    dirichlet_noise = np.random.dirichlet(alpha)
    policy = 0.75 * np.array(policy) + 0.25 * dirichlet_noise
    policy = np.array(policy)

    sqrt_total_plays = math.sqrt(total_weight)
    ucb_constant = math.log(
        (total_weight + C_BASE + 1) / C_BASE) + C_INIT
    def ucb(q, n, sqrtN, p):
      q_ratio = q/n if n > 0 else 0
      return q_ratio + ucb_constant*p*sqrtN/(1.0+n)

    value, index = max(
        (ucb(q[i], q_weight[i], sqrt_total_plays, policy[i]), i)
        for i in range(len(actions)))
    return index

  def run_simulation(self, state, policies, Q, N):
    # Stack of (visible_state, action) pairs visited along the path.
    visited = []
    current_player = state.current_player
    visible_state = state.getVisibleState()

    # Step 1: Traverse to unvisited state
    legal_actions = state.getLegalActions()
    while (visible_state in policies and
           legal_actions and
           not state.isEnd()):
      # All states are visited, use PUCT to pick next state
      action_index = self.pick_action_with_puct(
          visible_state, legal_actions, policies[visible_state], Q, N)
      action = legal_actions[action_index]
      visited.append((visible_state, action.id))

      state = state.generateSuccessor(action)
      visible_state = state.getVisibleState()
      legal_actions = state.getLegalActions()

    # Step 2: Get priors from the network
    value = 0  # Outcome value for current_player.
    count = 0  # How many games is value averaged over.
    if legal_actions and not state.isEnd():
      if self.nn.trained and state.train_deck.size() > 20 and state.current_player == current_player:
        policy, value = self.nn.predict(visible_state)
        policies[visible_state] = policy
        #print(np.array(policy).argsort()[-3:][::-1])
        if state.current_player != current_player:
          value = -value
        count = 1
      else:
        # Assign equal probabilities to each action
        policies[visible_state] = np.zeros(self.board.numActions())
        for action in legal_actions:
          policies[visible_state][action.id] = (
              1.0 / float(len(legal_actions)))

    # Step 3: Run one random rollout.
    if count == 0: # and legal_actions:
      #assert 1 == 2, '%s %s' % (str(legal_actions), str(state.isEnd()))
      state = state.deepCopy()
      while legal_actions and not state.isEnd():
        action = random.choice(legal_actions)
        state = state.generateSuccessor(action, copy_state=False)
        legal_actions = state.getLegalActions()

      winner, tie = getWinner(state)
      count += 1
      if not tie:
        if winner == current_player:
          value += 1
        else:
          value -= 1

    # Step 4: Backprop value to visited nodes.
    for s, a_idx in visited:
      if s.current_player == current_player:
        Q[(s, a_idx)] += value
      else:
        Q[(s, a_idx)] -= value
      N[(s, a_idx)] += count

  def simulationState(self, state, visible_state):
    if self.cheating:
      assert 1 == 2
      return state
    else:
      opponent_id = 1 - state.current_player
      guessed_state = GuessStateFromVisibleState(visible_state)
      if state.train_deck.size() != guessed_state.train_deck.size(): 
        print(state)
        print('Original: %s' % state.player_to_card_count[opponent_id])
        print(guessed_state)
        print('Guessed: %s' % state.player_to_card_count[opponent_id])
        print('Opponent card count: %s' % str(state.opponent_card_count))
        assert 1 == 2
      return guessed_state


def main(checkpoint_dir, tensorboard_dir, iterations, workers, episodes,
         datadir):
  train(
      checkpoint_dir,
      tensorboard_dir,
      num_iterations=iterations,
      num_workers=workers,
      episodes=episodes,
      datadir=datadir)

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Run mcts nn training.')
  parser.add_argument('--checkpoint_dir', type=str, default='~/checkpoint/',
      help='Directory to save checkpoints to.')
  parser.add_argument('--tensorboard_dir', type=str, default='~/tensorboard/',
      help='Directory to save Tensorboard summaries to.')
  parser.add_argument('-w', '--workers', type=int, default=10,
      help='Number of workers.')
  parser.add_argument('-e', '--episodes', type=int, default=40,
      help='Number of episodes.')
  parser.add_argument('-i', '--iterations', type=int, default=1000,
      help='Number of iterations.')
  parser.add_argument('-f', '--datadir', type=str, default='~/dataset')

  args = parser.parse_args()
  main(args.checkpoint_dir, args.tensorboard_dir,
       args.iterations, args.workers, args.episodes,
       os.path.expanduser(args.datadir))
