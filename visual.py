import collections
import math
import os
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.patches import Ellipse
from matplotlib import colors as mcolors
import numpy as np
from game_setup import *
import matplotlib.patheffects as pe

CITY_TO_COORDS = {
  CITY_VANCOUVER: (152, 120),
  CITY_CALGARY: (254, 105),
  CITY_WINNIPEG: (431, 113),
  CITY_SAULT_STE_MARIE: (621, 150),
  CITY_MONTREAL: (773, 99),
  CITY_TORONTO: (707, 170),
  CITY_SEATTLE: (149, 164),
  CITY_PORTLAND: (131, 201),
  CITY_HELENA: (333, 210),
  CITY_DULUTH: (522, 204),
  CITY_CHICAGO: (618, 255),
  CITY_PITTSBURGH: (720, 243),
  CITY_NEW_YORK: (789, 206),
  CITY_SALT_LAKE_CITY: (276, 306),
  CITY_DENVER: (380, 333),
  CITY_OMAHA: (497, 276),
  CITY_KANSAS_CITY: (515, 320),
  CITY_SAINT_LOUIS: (582, 320),
  CITY_NASHVILLE: (657, 350),
  CITY_WASHINGTON: (794, 275),
  CITY_RALEIGH: (749, 332),
  CITY_OKLAHOMA_CITY: (498, 387),
  CITY_SAN_FRANCISCO: (120, 357),
  CITY_LAS_VEGAS: (233, 396),
  CITY_SANTA_FE: (375, 404),
  CITY_LITTLE_ROCK: (570, 390),
  CITY_ATLANTA: (695, 378),
  CITY_CHARLESTON: (770, 384),
  CITY_LOS_ANGELES: (182, 441),
  CITY_PHOENIX: (276, 446),
  CITY_EL_PASO: (369, 476),
  CITY_DALLAS: (513, 458),
  CITY_HOUSTON: (548, 488),
  CITY_NEW_ORLEANS: (620, 479),
  CITY_MIAMI: (795, 507),
  CITY_BOSTON: (830, 147)
}

def center_and_radius(road):
  coords1 = CITY_TO_COORDS[road.city1]
  coords2 = CITY_TO_COORDS[road.city2]
  center = [(coords1[0] + coords2[0]) / 2.0,
            (coords1[1] + coords2[1]) / 2.0]
  diameter = math.sqrt((coords1[0] - coords2[0])**2 +
                       (coords1[1] - coords2[1])**2)
  angle = np.degrees(np.arctan((coords2[1] - coords1[1]) / float(coords2[0] - coords1[0])))
  return center, diameter, angle

COLOR_TO_PLT_COLOR = {
  COLOR_WHITE: "white",
  COLOR_BLACK: "black",
  COLOR_YELLOW: "yellow",
  COLOR_ORANGE: "orange",
  COLOR_BLUE: "blue",
  COLOR_RED: "red",
  COLOR_PINK: "pink",
  COLOR_GREEN: "darkgreen",
  COLOR_GRAY: "gray"
}

def pltColor(color):
  return COLOR_TO_PLT_COLOR[color]

ROAD_STATE_TO_TYPE = {
  -1: "solid",
  0: "solid",
  1: "dashed",
  2: "dotted"
}

def shift(segment):
  s = np.zeros((2,), dtype=np.float32)
  s[0] = segment[1][1] - segment[0][1]
  s[1] = segment[0][0] - segment[1][0]
  return s / np.linalg.norm(s)

def plotMap(
    road_state, destinations=None, color_cards=None, current_player=0, action=None):
  if destinations == None:
    destinations = []

  fig, ax = plt.subplots()
  ax.invert_yaxis()
  ax.axis('off')
  line_border = [pe.Stroke(linewidth=3, foreground='black'), pe.Normal()]

  x = [CITY_TO_COORDS[i][0] for i in range(CITY_COUNT)]
  y = [CITY_TO_COORDS[i][1] for i in range(CITY_COUNT)]

  board = getDefaultBoard()
  roads = board.roads
  segments = []
  colors = []
  style = []

  for road in roads:
    segment = np.array([CITY_TO_COORDS[road.city1], CITY_TO_COORDS[road.city2]], dtype=np.float32)
    if road.sibling is not None:
      s = 3.5*shift(segment)
      if road.id >= road.sibling.id:
        s = -s
      segment += s
    segments.append(segment)
    colors.append(pltColor(road.color))
    style.append(ROAD_STATE_TO_TYPE[road_state[road.id]])

  lines = LineCollection(
      segments, colors=colors, linewidths=2, zorder=1, path_effects=line_border,
      linestyle=style)
  ax.add_collection(lines)
  ax.scatter(x, y, color="white", zorder=10, edgecolors="black")

  coord_to_dest_num = collections.defaultdict(list)
  for i, destination in enumerate(destinations):
    fillcolor = "white" #"lightblue" if current_player == 0 else "pink"
    bordercolor = "black" #if current_player == 0 else "red"
    city1coord = CITY_TO_COORDS[destination.city1]
    city2coord = CITY_TO_COORDS[destination.city2]
    coord_to_dest_num[city1coord].append(str(i))
    coord_to_dest_num[city2coord].append(str(i))

  for coord, destinations in coord_to_dest_num.items():
    label = ','.join(destinations)
    ax.scatter([coord[0]], [coord[1]],
               s=160,
               color=fillcolor,
               zorder=11,
               edgecolors=bordercolor)
    ax.text(coord[0], coord[1], label,
            horizontalalignment="center", verticalalignment="center",
            fontsize=6, zorder=12)

  ax.text(100, 530, "Player 1", color="black", weight="semibold")
  ax.text(600, 530, "Player 2", color="black", weight="semibold")
  ax.plot([100, 250], [540, 540], linewidth=2, path_effects=line_border,
          linestyle="dashed", color="black")
  ax.plot([600, 750], [540, 540], linewidth=2, path_effects=line_border,
          linestyle="dotted", color="black")

  if color_cards:
    xstart = 110 if current_player == 0 else 610
    cardsize = 120
    x = [xstart + (30) * i for i in range(COLOR_COUNT)] 
    c = [pltColor(i) for i in range(COLOR_COUNT)]
    ax.scatter(x, [570] * len(x), s=cardsize, marker="s", edgecolor="black",
               color=c)
    for i, count in enumerate(color_cards):
      ax.text(x[i], 600, count,
              horizontalalignment="center",
              fontsize=6, zorder=12)

  if action is not None:
    xstart = 100 if current_player == 0 else 600
    c = "black" #"blue" if current_player == 0 else "red"
    ax.text(xstart, 625, "Action: %s" % str(action.action_type.name), color=c)
    if action.action_type == ActionType.CLAIM_ROUTE:
      center, diameter, angle = center_and_radius(action.road)
      c = pltColor(action.road.color)
      c = "gray" if c == "white" or c == "black" else c
      circle = Ellipse(center, width=diameter, height=30, angle=angle,
                       color=c, zorder=0)
      ax.add_artist(circle)
    
def plotMapForStateAndAction(visible_s, action, filepath):
  plotMap(visible_s.route_state,
          [DESTINATION_CARDS[i] for i in visible_s.destination_cards],
          visible_s.train_cards,
          visible_s.current_player,
          action)
  plt.savefig(filepath)


class GameReport:
  def __init__(self, dirpath, player1label, player2label):
    self.dirpath = dirpath
    self.size = 0
    self.player1label = player1label
    self.player2label = player2label

  def add(self, visible_s, action):
    filepath = os.path.join(self.dirpath, "%d.png" % self.size)
    plotMapForStateAndAction(visible_s, action, filepath) 
    self.size += 1

  def build(self, scores):
    html = """<!DOCTYPE html>
<html>
<head>
<style>
body {
  font-family: Verdana, Arial;
}
</style>
</head>
<body>
<h1>%s vs %s</h1>
""" % (self.player1label, self.player2label)

    for i in range(self.size):
      html += """<div>Step %d</div>
<img src="%d.png">
""" % (i, i)
    html += """
<h2>Scores: %d, %d</h2>
</body>
</html>
""" % (scores[0], scores[1])
    filepath = os.path.join(self.dirpath, "report.html")

    with open(filepath, "w") as f:
      f.write(html)
    return filepath
      

def main():
  board = getDefaultBoard()
  road_state = np.zeros((len(board.roads),))
  road_state[0] = 1
  road_state[10] = 1
  road_state[11] = 1
  road_state[12] = 2
  road_state[20] = 2
  color_cards = [0] * COLOR_COUNT
  color_cards[5] = 12
  action = Action(ActionType.DRAW_TRAIN_CARD)
  action = ClaimRoadAction(ActionType.CLAIM_ROUTE, board.roads[6])
  plotMap(road_state, [DESTINATION_CARDS[0]], color_cards, 0, action)
  plt.show()

if __name__ == "__main__":
  main()
