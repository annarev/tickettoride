# Utilities for collecting and converting examples between formats.
from game_setup import *
import numpy as np

NUM_FEATURES = 40
NUM_ARRAY_FEATURES = 5


class Examples(object):
  def __init__(self):
    # List of tuples in the form
    # (visible_state, policy, value)
    self.examples = []

  def add(self, visible_state, policy):
    self.examples.append([visible_state, policy, None])

  def update_outcomes(self, winner, tie, Q, N, num_actions):
    for example in self.examples:
      state = example[0]
      q = [Q.get((state, a), -1) for a in range(num_actions)]
      n = [N.get((state, a), 1) for a in range(num_actions)]
      expected_q = max([w/float(g) for w, g in zip(q, n)])
      #outcome = 0
      #if state.current_player == winner:
      #  outcome = 1
      #elif tie:
      #  outcome = 0
      #else:
      #  outcome = -1
      #example[-1] = outcome # 0.5*expected_q + 0.5*outcome
      example[-1] = expected_q


def getRoadOwner(road, route_state):
  if route_state[road.id] == 1 or route_state[road.id] == 2:
    return route_state[road.id] - 1
  return -1

def getColorCardCount(road, train_cards):
  # Check how many cards do we have of required
  # color for this road.
  if road.color == COLOR_GRAY:
    # If road is gray, we can use any color cards.
    return max(train_cards)
  return train_cards[road.color]

def normalize(data):
  return (data - data.mean()) / data.std()

def normalizeColorCount(data):
  # Maximum number of cards of a certain color is 12
  return (data - 6) / 12.0

def encode_graph_state(state):
  board = state.board
  num_edges = len(board.roads)

  # Feature columns
  current_player = np.zeros((num_edges, 2), dtype=np.float32)
  road_distance = np.zeros((num_edges, 1), dtype=np.float32)
  road_owner = np.zeros((num_edges, 2), dtype=np.float32)
  destination_cards1 = np.zeros(
      (num_edges, len(DESTINATION_CARDS)), dtype=np.float32)
  card_count1 = np.zeros((num_edges, 1), dtype=np.float32)
  enough_trains = np.zeros((num_edges, 1), dtype=np.float32)
  route_available = np.zeros((num_edges, 1), dtype=np.float32)
  discarded_cards = np.zeros((num_edges, 1), dtype=np.float32)
  bias = np.ones((num_edges, 1), dtype=np.float32)

  for i, road in enumerate(board.roads):
    current_player[i][state.current_player] = 1
    road_distance[i][0] = road.distance
    owner = getRoadOwner(road, state.route_state)
    if owner >= 0:
      road_owner[i][owner] = 1

    # Destination card id
    for destination_card_id in state.destination_cards:
      destination = DESTINATION_CARDS[destination_card_id]
      if (destination.city1 == road.city1 or
          destination.city2 == road.city2 or
          destination.city1 == road.city2 or
          destination.city2 == road.city1):
        destination_cards1[i][destination_card_id] = destination.reward / 22.0 # max destination reward is 22

    # Card count of required color
    card_count1[i][0] = getColorCardCount(road, state.train_cards)
    # "card counting"
    discarded_cards[i][0] = state.discarded_cards[road.color]

    if state.player_to_train_count[state.current_player] >= road.distance:
      enough_trains[i][0] = 1

    if state.route_state[i] == 0:
      route_available[i][0] = 1

  edge_features = np.concatenate([
      current_player,
      normalize(road_distance),
      road_owner,
      destination_cards1,
      normalizeColorCount(card_count1),
      enough_trains,
      normalizeColorCount(discarded_cards),
      route_available,
      bias
  ], axis=1)
  assert edge_features.shape == (num_edges, NUM_FEATURES)
  return edge_features


def encode_array_features(state):
  # Other features:
  # Train count: 
  #   1 slot for number of trains for each player (total 2 slots)
  # Train card deck size:
  #   1 slot
  # Current player id:
  #   2 slots
  # TODO: self.last_round_counter = last_round_counter

  assert NUM_ARRAY_FEATURES == 5
  other_features = np.zeros((NUM_ARRAY_FEATURES,), dtype=np.float32)
  for player, train_count in enumerate(state.player_to_train_count):
    other_features[player] = train_count / 45.0
  other_features[2] = state.train_deck_size / (8.0*12.0)
  other_features[3+state.current_player] = 1
  return other_features   

def encode_game_state(state):
  edge_features = encode_graph_state(state)
  array_features = encode_array_features(state)
  return edge_features, array_features

